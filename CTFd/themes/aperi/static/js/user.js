chartColors = [
	'rgb(255, 99, 132)',
	'rgb(153, 102, 255)',
	'rgb(255, 159, 64)',
	'rgb(255, 159, 64)',
	'rgb(255, 205, 86)',
	'rgb(75, 192, 192)',
	'rgb(54, 162, 235)',
	'rgb(255, 99, 132)',
	'rgb(201, 203, 207)',
	'rgb(255, 205, 86)'
];

var failChartData = {
		datasets: [{}],
		labels: [
			'Validations',
			'Erreurs'
		]
	};

var configctxfail = {
	type: 'doughnut',
	data: failChartData,
	options: {
		responsive: true,
		hoverMode: 'index',
		stacked: false,
		legend: {
			position: 'top',
		},
		title: {
			display: true
		},
		animation: {
			animateScale: true,
			animateRotate: true
		}
	}
};


	var ctx = document.getElementById("failChart").getContext('2d');
	window.myLineFail = Chart.Doughnut(ctx, configctxfail);

function updateGraph(){
	var base_url = script_root + '/api/v1/users/' + user_account_id;
	$.get(base_url + '/fails', function (fails) {
		$.get(base_url + '/solves', function (solves) {
			var solves_count = solves.data.length;
			var fails_count = fails.meta.count;

			failChartData.datasets = [];
			failChartData.datasets.push({
			data: [solves_count,fails_count],
			backgroundColor: [
				'rgb(65, 244, 116)',
				'rgb(244, 66, 66)'
			]
		});
			window.myLineFail.update();
		});
	});
}


var challChartData = {
		datasets: [/*{
			data: [0,0],
			backgroundColor: [
				'rgb(65, 244, 116)',
				'rgb(244, 66, 66)'
			]
		}*/],
		labels: [/*
			'Validations',
			'Erreurs'
		*/]
	};

var configctxchall = {
	type: 'doughnut',
	data: challChartData,
	options: {
		responsive: true,
		hoverMode: 'index',
		stacked: false,
		legend: {
			position: 'top',
		},
		title: {
			display: true
		},
		animation: {
			animateScale: true,
			animateRotate: true
		}
	}
};


var ctxchall = document.getElementById("challChart").getContext('2d');
window.myLineChall = Chart.Doughnut(ctxchall, configctxchall);


function updateGraphChall(){
	$.get(script_root + '/api/v1/users/' + user_account_id + '/solves', function (response) {
		var solves = response.data;

		var categories = [];
		for (var i = 0; i < solves.length; i++) {
			categories.push(solves[i].challenge.category)
		}

		var keys = categories.filter(function (elem, pos) {
			return categories.indexOf(elem) == pos;
		});

		var counts = [];
		for (var i = 0; i < keys.length; i++) {
			var count = 0;
			for (var x = 0; x < categories.length; x++) {
				if (categories[x] == keys[i]) {
					count++;
				}
			}
			counts.push(count)
		}

		var bgcolors = []
		for (var j = 0; j < counts.length; j++){
			bgcolors.push(chartColors[j%chartColors.length]);
		}
		challChartData.datasets = [];
		challChartData.labels = [];
		challChartData.datasets.push({
			data: counts,
			backgroundColor: bgcolors
		});
		challChartData.labels = keys;
		
		window.myLineChall.update();
	});
}

/************************/

var scoreLineChartData = {};

var configctxscore = {
	data: scoreLineChartData,
	options: {
		elements: {
			line: {
				tension: 0.2
			}
		},
		responsive: true,
		hoverMode: 'index',
		stacked: false,
		scales: {
			yAxes: [{
				type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
				display: true,
				position: 'left',
				id: 'y-axis-1',
				ticks: { beginAtZero: true }
			}],
			xAxes: [{
				type: 'time',
				time: {
					tooltipFormat: 'D MMM h:mm',
					scaleBeginAtZero : true,
					max: new Date()
				}
			}]
		}
	}
}

window.onload = function() {
	var ctxscore = document.getElementById("scoreChart").getContext('2d');
	window.myLineScore = Chart.Line(ctxscore, configctxscore);
};


function updateGraphScore(){
	var times = [];
	var scores = [];
	if (self)
	$.get(script_root + '/api/v1/users/' + user_account_id + '/solves', function (solve_data) {
		$.get(script_root + '/api/v1/users/' + user_account_id + '/awards', function (award_data) {
			var solves = solve_data.data;
			var awards = award_data.data;

			var total = solves.concat(awards);

			total.sort(function (a, b) {
				return new Date(a.date) - new Date(b.date);
			});

			for (var i = 0; i < total.length; i++) {
				var date = moment(total[i].date);
				times.push(date.toDate());
				try {
					scores.push(total[i].challenge.value);
				} catch (e) {
					scores.push(total[i].value);
				}
			}
			scores = cumulativesum(scores);
			//x: times,
			//y: scores,

			var ddata = [];
			ddata.push({
				x:times[0], // Debut du CTF
				y:0 // Debut du CTF
			});
			//console.dir(times);
			for(var j = 0; j < scores.length; j++){
				ddata.push({
					x:times[j], // Debut du CTF
					y:scores[j] // Debut du CTF
				});
			}
			
			var newDataset = {
				label: "Équipe",
				backgroundColor: chartColors[0],
				borderColor: chartColors[0],
				data: ddata,
				fill: false
			};
            //console.dir(newDataset);
            scoreLineChartData.datasets = [];
			scoreLineChartData.datasets.push(newDataset);
			window.myLineScore.update();

		});
	});
}



function update(){
  updateGraph();
  updateGraphChall();
  updateGraphScore();
}

setInterval(update, 30000); // Update scores every 5 min
update();
